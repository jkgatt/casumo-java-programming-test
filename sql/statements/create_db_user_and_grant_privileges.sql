DROP USER 'casumo'@'localhost';

CREATE USER 'casumo'@'localhost' 
	IDENTIFIED BY 'casumo123';

GRANT SELECT,INSERT,UPDATE,DELETE 
	ON `casumo_video_rental_store`.* 
    TO 'casumo'@'localhost'; 