# film price and film type data

INSERT INTO `casumo_video_rental_store`.`film_price`
	(`name`, `price`)
VALUES
	("Premium", 40),
    ("Basic", 30);
    
INSERT INTO `casumo_video_rental_store`.`film_type`
	(`name`, `price_id`)
VALUES
	("New Release", 1),
    ("Regular Film", 2),
	("Old Film", 2);