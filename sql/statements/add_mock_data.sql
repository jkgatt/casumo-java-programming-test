INSERT INTO `casumo_video_rental_store`.`customer`
	(`bonus_points`)
VALUES
	(0),
    (0),
    (0),
    (0),
    (0);
    
INSERT INTO `casumo_video_rental_store`.`film`
	(`name`,
    `quantity`,
	`type_id`)
VALUES
    ("Dunkirk", 2, 1),
    ("Get Out", 3, 1),
    ("12 Years a Slave", 1, 2),
    ("Toy Story 3", 1, 2),
    ("The Godfather", 4, 3),
    ("The Wizard of Oz", 1, 3);
