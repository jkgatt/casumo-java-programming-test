package com.jeangatt.video_rental_store.db;

import com.jeangatt.video_rental_store.core.*;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;

public class RentalDAOTest {

    @Rule
    public DAOTestRule database = DAOTestRule.newBuilder()
            .addEntityClass(Rental.class)
            .addEntityClass(Film.class)
            .addEntityClass(FilmType.class)
            .addEntityClass(FilmPrice.class)
            .addEntityClass(Customer.class)
            .build();

    private RentalDAO rentalDAO;

    @Before
    public void setup() throws Exception {
        rentalDAO = new RentalDAO(database.getSessionFactory());
    }

    // Testing the creation (rental) or update (rental) of a Rental to the database
    @Test
    public void createOrUpdateRentalTest() {
        Rental rental = new Rental();

        int rentalId = database.inTransaction(()-> {
            return rentalDAO.createOrUpdate(rental);
        });

        assertNotNull(rentalId);
    }

    // Testing the findById method with a rental id of 1
    @Test
    public void findByIdTest(){
        Optional<Rental> possibleRental = database.inTransaction(() -> {
            return rentalDAO.findById(1);
        });

        assertNotNull(possibleRental);
    }

    //Testing the totalRentalFilmsByFilmId method with a film Id of 8
    @Test
    public void totalRentalFilmsByFilmIdTest() {
        long totalFilms = database.inTransaction(() -> {
            return rentalDAO.totalRentalsByFilmId(8);
        });

        assertNotNull(totalFilms);
    }

    // Testing the findByCustomerIdAndFilmId method, with customer ID of 3 and film ID of 8
    @Test
    public void findByCustomerIdandFilmIdTest(){
        List<Rental> rentals = database.inTransaction(() -> {
            return rentalDAO.findByCustomerIdAndFilmID(3, 8);
        });

        assertNotNull(rentals);
    }

}
