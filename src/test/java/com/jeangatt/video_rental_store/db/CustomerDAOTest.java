package com.jeangatt.video_rental_store.db;

import com.jeangatt.video_rental_store.core.Customer;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

public class CustomerDAOTest {

    @Rule
    public DAOTestRule database =
            DAOTestRule.newBuilder().addEntityClass(Customer.class).build();

    private CustomerDAO customerDAO;

    @Before
    public void setup() throws Exception {
        customerDAO = new CustomerDAO(database.getSessionFactory());
    }

    // Testing the creation of a User to the database
    @Test
    public void createOrUpdateCustomerTest(){
        Customer customer = new Customer();

        int customerId = database.inTransaction(() -> {
            return customerDAO.createOrUpdateCustomer(customer);
        });

        assertNotNull(customer.getId());
    }

    // Testing the findById method with a customer id of 1
    @Test
    public void findByIdTest(){

        Optional<Customer> possibleCustomer = database.inTransaction(() -> {
            return customerDAO.findById(1);
        });

        assertNotNull(possibleCustomer);
    }
}
