package com.jeangatt.video_rental_store.db;

import com.jeangatt.video_rental_store.core.FilmPrice;
import com.jeangatt.video_rental_store.core.FilmType;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;

public class FilmTypeDAOTest {

    @Rule
    public DAOTestRule database = DAOTestRule.newBuilder()
            .addEntityClass(FilmType.class)
            .addEntityClass(FilmPrice.class)
            .build();

    private FilmTypeDAO filmTypeDAO;

    @Before
    public void setup() throws Exception{
        filmTypeDAO = new FilmTypeDAO(database.getSessionFactory());
    }

    // Testing the findById method with a film type id of 1
    @Test
    public void findByIdTest(){

        Optional<FilmType> possibleFilmType = database.inTransaction(() -> {
            return filmTypeDAO.findById(1);
        });

        assertNotNull(possibleFilmType);
    }

}
