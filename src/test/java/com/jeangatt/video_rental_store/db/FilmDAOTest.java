package com.jeangatt.video_rental_store.db;

import com.jeangatt.video_rental_store.core.Customer;
import com.jeangatt.video_rental_store.core.Film;
import com.jeangatt.video_rental_store.core.FilmPrice;
import com.jeangatt.video_rental_store.core.FilmType;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;

public class FilmDAOTest {

    @Rule
    public DAOTestRule database = DAOTestRule.newBuilder()
            .addEntityClass(Film.class)
            .addEntityClass(FilmType.class)
            .addEntityClass(FilmPrice.class)
            .build();

    private FilmDAO filmDAO;

    @Before
    public void setup() throws Exception {
        filmDAO = new FilmDAO(database.getSessionFactory());
    }

    // Testing the creation of a film in the database
    @Test
    public void createOrUpdateFilmTest(){
        Film film = new Film("Test Film");

        int filmId = database.inTransaction(() -> {
            return filmDAO.createOrUpdate(film);
        });

        assertNotNull(filmId);
    }

    // Testing the findById method with a film id of 1
    @Test
    public void findByIdTest(){

        Optional<Film> possibleFilm = database.inTransaction(() -> {
            return filmDAO.findById(1);
        });

        assertNotNull(possibleFilm);
    }

    // Testing the findByWildCardName method with a name of "Laugh"
    @Test
    public void findByWildcardNameTest(){
        String name = "Laugh";

        List<Film> films = database.inTransaction(() -> {
            return filmDAO.findByWildcardName(name);
        });

        assertNotNull(films);
    }
    // Testing the findByExactName method with a name of "Dunkirk"
    @Test
    public void findByExactNameTest(){
        String name = "Dunkirk";

        List<Film> films = database.inTransaction(() -> {
            return filmDAO.findByExactName(name);
        });

        assertNotNull(films);
    }
}
