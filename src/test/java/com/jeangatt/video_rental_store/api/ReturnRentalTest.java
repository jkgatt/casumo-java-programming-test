package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.junit.Assert.assertEquals;

public class ReturnRentalTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    // AddRental instance
    private ReturnRental returnRental;

    @Before
    public void setup(){
        ArrayList<FilmReturnDetails> filmReturnDetails = new ArrayList<>();
        filmReturnDetails.add(new FilmReturnDetails("Third Film"));
        filmReturnDetails.add(new FilmReturnDetails("Fourth Film"));
        filmReturnDetails.add(new FilmReturnDetails("Fifth Film"));

        returnRental = new ReturnRental(5, "25/08/2017", filmReturnDetails);
    }

    @Test
    public void serializesToJson() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/return_rental.json"), ReturnRental.class)
        );

        assertEquals(MAPPER.writeValueAsString(returnRental), expected);
    }

    @Test
    public void deserializesFromJson() throws Exception {
        assertEquals(
                MAPPER.readValue(fixture("fixtures/return_rental.json"), ReturnRental.class),
                returnRental
        );
    }
}