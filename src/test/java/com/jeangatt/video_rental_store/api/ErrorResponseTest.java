package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.junit.Assert.assertEquals;

public class ErrorResponseTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    // ErrorResponse instance
    private ErrorResponse errorResponse;

    @Before
    public void setup(){
        errorResponse = new ErrorResponse(404, "An error has occured...");
    }

    @Test
    public void serializesToJson() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/error_response.json"), ErrorResponse.class)
        );

        assertEquals(MAPPER.writeValueAsString(errorResponse), expected);
    }

    @Test
    public void desializesFromJson() throws Exception {
        assertEquals(
                MAPPER.readValue(fixture("fixtures/error_response.json"), ErrorResponse.class),
                errorResponse
        );
    }
}
