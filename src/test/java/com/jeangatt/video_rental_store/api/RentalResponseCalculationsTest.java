package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.junit.Assert.assertEquals;

public class RentalResponseCalculationsTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    // RentalReponseCalculations instance
    private RentalResponseCalculations rentalResponseCalculations;

    @Before
    public void setup(){
        ArrayList<FilmRentalDetails> filmRentalDetails = new ArrayList<>();
        filmRentalDetails.add(new FilmRentalDetails("Laugh Out", 5, BigDecimal.valueOf(240)));
        filmRentalDetails.add(new FilmRentalDetails("Loud", 8, BigDecimal.valueOf(260)));

        rentalResponseCalculations = new RentalResponseCalculations(4, filmRentalDetails, 500);
    }

    @Test
    public void serializesToJson() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/rental_response_calculations.json"),
                        RentalResponseCalculations.class)
        );

        assertEquals(MAPPER.writeValueAsString(rentalResponseCalculations), expected);
    }

    @Test
    public void deserilizesFromJson() throws Exception {
        assertEquals(
                MAPPER.readValue(fixture("fixtures/rental_response_calculations.json"),
                        RentalResponseCalculations.class),
                rentalResponseCalculations
        );
    }

}
