package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.junit.Assert.assertEquals;


public class AddFilmTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    // addFilm instance
    private AddFilm addFilm;

    @Before
    public void setup(){
        addFilm = new AddFilm("The best film ever", 3);
    }

    @Test
    public void serializesToJson() throws Exception {
        final String expected = MAPPER.writeValueAsString(
          MAPPER.readValue(fixture("fixtures/add_film.json"), AddFilm.class)
        );

        assertEquals(MAPPER.writeValueAsString(addFilm), expected);
    }

    @Test
    public void deserializesFromJson() throws Exception{
        assertEquals(
                MAPPER.readValue(fixture("fixtures/add_film.json"), AddFilm.class),
                addFilm
        );
    }
}
