package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.junit.Assert.assertEquals;

public class ReturnResponseCalculationsTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    // ReturnReponseCalculations instance
    private ReturnResponseCalculations returnResponseCalculations;

    @Before
    public void setup(){
        ArrayList<FilmReturnDetails> filmReturnDetails = new ArrayList<>();
        filmReturnDetails.add(new FilmReturnDetails("Hello World", 2, BigDecimal.valueOf(80)));
        filmReturnDetails.add(new FilmReturnDetails("Bye World", 5, BigDecimal.valueOf(150)));

        returnResponseCalculations = new ReturnResponseCalculations(5, filmReturnDetails, 240);
    }

    @Test
    public void serializesToJson() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/return_response_calculations.json"),
                        ReturnResponseCalculations.class)
        );

        assertEquals(MAPPER.writeValueAsString(returnResponseCalculations), expected);
    }

    @Test
    public void deserilizesFromJson() throws Exception {
        assertEquals(
                MAPPER.readValue(fixture("fixtures/return_response_calculations.json"),
                        ReturnResponseCalculations.class),
                returnResponseCalculations);
    }
}
