package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.junit.Assert.assertEquals;

public class AddRentalTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    // AddRental instance
    private AddRental addRental;

    @Before
    public void setup(){
        ArrayList<FilmRentalDetails> filmRentalDetails = new ArrayList<>();
        filmRentalDetails.add(new FilmRentalDetails("First Film", 4));
        filmRentalDetails.add(new FilmRentalDetails("Second Film", 6));

        addRental = new AddRental(10, "25/08/2017", filmRentalDetails);
    }

    @Test
    public void serializesToJson() throws Exception {
        final String expected = MAPPER.writeValueAsString(
          MAPPER.readValue(fixture("fixtures/add_rental.json"), AddRental.class)
        );

        assertEquals(MAPPER.writeValueAsString(addRental), expected);
    }

    @Test
    public void deserializesFromJson() throws Exception {
        assertEquals(
                MAPPER.readValue(fixture("fixtures/add_rental.json"), AddRental.class),
                addRental
        );
    }
}
