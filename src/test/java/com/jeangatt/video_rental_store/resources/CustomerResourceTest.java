package com.jeangatt.video_rental_store.resources;

import com.jeangatt.video_rental_store.core.Customer;
import com.jeangatt.video_rental_store.db.CustomerDAO;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.*;

import javax.ws.rs.core.Response;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CustomerResourceTest {

    private static final CustomerDAO customerDAO = mock(CustomerDAO.class);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new CustomerResource(customerDAO)).build();

    private final Customer customer = new Customer();

    @Before
    public void setup(){
        when(customerDAO.findById(any(Integer.class))).thenReturn(Optional.ofNullable(customer));
        when(customerDAO.createOrUpdateCustomer(any(Customer.class))).thenReturn(1);
    }

    @After
    public void tearDown(){
        reset(customerDAO);
    }

    // Test to check whether the findById resource is working
    @Test
    public void findByIdTest() {
        int randomId = any(Integer.class);

        final Customer customerFound = resources.client()
                .target("/customers/" + randomId)
                .request()
                .get(Customer.class);

        assertEquals(customerFound, customer);

        verify(customerDAO).findById(randomId);
    }

    // Test to check whether the addCustomer resource is working
    @Test
    public void addCustomerCorrectlyTest() {
        Response addCustomerResponse = resources.client()
                .target("/customers")
                .request()
                .post(null);

        assertEquals(addCustomerResponse.getStatus(), Response.Status.OK.getStatusCode());

        verify(customerDAO).createOrUpdateCustomer(customer);
    }
}
