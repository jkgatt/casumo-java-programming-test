package com.jeangatt.video_rental_store.resources;

import com.jeangatt.video_rental_store.api.AddFilm;
import com.jeangatt.video_rental_store.core.Film;
import com.jeangatt.video_rental_store.core.FilmPrice;
import com.jeangatt.video_rental_store.core.FilmType;
import com.jeangatt.video_rental_store.db.FilmDAO;
import com.jeangatt.video_rental_store.db.FilmTypeDAO;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class FilmResourceTest {

    private static final FilmDAO filmDAO = mock(FilmDAO.class);
    private static final FilmTypeDAO filmTypeDAO = mock(FilmTypeDAO.class);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule
            .builder()
            .addResource(new FilmResource(filmDAO, filmTypeDAO))
            .build();

    private final FilmPrice filmPrice = new FilmPrice();
    private final FilmType filmType = new FilmType();
    private final Film film = new Film();
    private final List<Film> films = new ArrayList<>();

    @Before
    public void setup(){
        // Setup Film, FilmType and FilmPrice
        film.setType(filmType);

        filmType.setPrice(filmPrice);

        // Setting up mock methods
        when(filmDAO.findById(any(Integer.class))).thenReturn(Optional.ofNullable(film));
        when(filmDAO.findByExactName(any(String.class))).thenReturn(films);
        when(filmDAO.createOrUpdate(any(Film.class))).thenReturn(1);

        when(filmTypeDAO.findById(any(Integer.class))).thenReturn(Optional.ofNullable(filmType));
    }

    @After
    public void tearDown(){
       reset(filmDAO);
       reset(filmTypeDAO);
    }

    // Test to check whether the findById resource is working
    @Test
    public void findByIdTest(){
        int randomId = any(Integer.class);

        final Film filmFound = resources.client()
                .target("/films/" + randomId)
                .request()
                .get(Film.class);

        assertEquals(filmFound, film);

        verify(filmDAO).findById(randomId);
    }

    // Test to check whether the addFilm resource is working
    @Test
    public void addFilmCorrectlyTest() throws Exception{

        AddFilm addFilm = new AddFilm("The best film ever", 3);

        final Response addFilmResponse = resources.client()
                .target("/films")
                .request()
                .post(Entity.entity(addFilm, MediaType.APPLICATION_JSON_TYPE));


        assertEquals(addFilmResponse.getStatus(), Response.Status.OK.getStatusCode());

        verify(filmDAO).findByExactName("The best film ever");
        verify(filmTypeDAO).findById(3);
    }
}
