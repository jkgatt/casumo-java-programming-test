package com.jeangatt.video_rental_store.resources;

import com.jeangatt.video_rental_store.api.AddRental;
import com.jeangatt.video_rental_store.api.FilmRentalDetails;
import com.jeangatt.video_rental_store.api.FilmReturnDetails;
import com.jeangatt.video_rental_store.api.ReturnRental;
import com.jeangatt.video_rental_store.core.*;
import com.jeangatt.video_rental_store.db.CustomerDAO;
import com.jeangatt.video_rental_store.db.FilmDAO;
import com.jeangatt.video_rental_store.db.RentalDAO;
import com.sun.org.apache.regexp.internal.RE;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.swing.text.html.Option;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class RentalResourceTest {

    private static final RentalDAO rentalDAO = mock(RentalDAO.class);
    private static final FilmDAO filmDAO = mock(FilmDAO.class);
    private static final CustomerDAO customerDAO = mock(CustomerDAO.class);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new RentalResource(rentalDAO, filmDAO, customerDAO))
            .build();

    private final FilmPrice filmPrice = new FilmPrice();
    private final FilmType filmType = new FilmType();
    private final Film film = new Film();
    private final ArrayList<Film> films = new ArrayList<>();
    {
        // Setting up the films list
        filmPrice.setPrice(new BigDecimal(50));
        filmType.setPrice(filmPrice);

        filmType.setId(1);

        Film firstFilm = new Film("The Laugh out");
        firstFilm.setType(filmType);

        Film secondFilm = new Film("Walking out");
        secondFilm.setType(filmType);

        films.add(firstFilm);
        films.add(secondFilm);
    }

    private final Customer customer = new Customer();

    private final Rental rental = new Rental();
    private final ArrayList<Rental> rentals = new ArrayList<>();
    {
        // Setting up the rentals list
        Rental rental = new Rental(
                LocalDate.parse("27/08/2017", DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                10,
                new BigDecimal(300),
                customer,
                film
        );

        rentals.add(rental);
    }

    @Before
    public void setup(){

        //Setting up the the rental instance
        filmType.setPrice(filmPrice);
        film.setType(filmType);
        rental.setFilm(film);
        rental.setCustomer(customer);

        // Setting up mock methods
        when(rentalDAO.findById(any(Integer.class))).thenReturn(Optional.ofNullable(rental));
        when(rentalDAO.totalRentalsByFilmId(any(Integer.class))).thenReturn(new Long(0));
        when(rentalDAO.createOrUpdate(any(Rental.class))).thenReturn(1);
        when(rentalDAO.findByCustomerIdAndFilmID(any(Integer.class), any(Integer.class))).thenReturn(rentals);

        when(customerDAO.findById(any(Integer.class))).thenReturn(Optional.ofNullable(customer));

        when(filmDAO.findByExactName(any(String.class))).thenReturn(films);
    }

    @After
    public void tearDown(){
        reset(rentalDAO);
        reset(filmDAO);
        reset(customerDAO);
    }

    // Test to check whether the findById resouce is working
    @Test
    public void findByIdTest(){
        int randomId = any(Integer.class);

        final Rental rentalFound  = resources.client()
                .target("/rentals/"+ randomId)
                .request()
                .get(Rental.class);

        assertEquals(rentalFound, rental);

        verify(rentalDAO).findById(randomId);
    }

    // Test to check whether the addRental resource is working correctly
    @Test
    public void addRentalCorrectlyTest(){
        ArrayList<FilmRentalDetails> filmRentalDetails = new ArrayList<>();

        filmRentalDetails.add(new FilmRentalDetails("The Laugh Out", 3));
        filmRentalDetails.add(new FilmRentalDetails("Walking Out", 7));

        AddRental addRental = new AddRental(1, "27/08/2017", filmRentalDetails);

        final Response rentalResponse = resources.client()
                .target("/rentals")
                .request()
                .post(Entity.entity(addRental, MediaType.APPLICATION_JSON));

        assertEquals(rentalResponse.getStatus(), Response.Status.OK.getStatusCode());

        verify(customerDAO).findById(1);
        verify(filmDAO).findByExactName("The Laugh Out");
        verify(filmDAO).findByExactName("Walking Out");
        verify(rentalDAO, times(2)).totalRentalsByFilmId(0);
    }

    // Test to check whether the addRental resource is working correctly when an incorrectly formatted date is provided
    @Test
    public void addRentalIncorrectDateTest(){
        ArrayList<FilmRentalDetails> filmRentalDetails = new ArrayList<>();

        filmRentalDetails.add(new FilmRentalDetails("The Laugh Out", 3));

        AddRental addRental = new AddRental(1, "/08/2017", filmRentalDetails);

        final Response rentalResponse = resources.client()
                .target("/rentals")
                .request()
                .post(Entity.entity(addRental, MediaType.APPLICATION_JSON));

        assertEquals(rentalResponse.getStatus(), Response.Status.BAD_REQUEST.getStatusCode());
        verify(customerDAO).findById(1);
    }

    // Test to check whether the addRental resource is working correctly
    @Test
    public void returnRentalCorrectlyTest(){
        ArrayList<FilmReturnDetails> filmReturnDetails = new ArrayList<>();

        filmReturnDetails.add(new FilmReturnDetails("The Laugh Out"));
        filmReturnDetails.add(new FilmReturnDetails("Walking Out"));

        ReturnRental returnRental = new ReturnRental(1, "30/08/2017", filmReturnDetails);

        final Response rentalResponse = resources.client()
                .target("/rentals")
                .request()
                .put(Entity.entity(returnRental, MediaType.APPLICATION_JSON));

        assertEquals(rentalResponse.getStatus(), Response.Status.OK.getStatusCode());

        verify(customerDAO).findById(1);
        verify(filmDAO).findByExactName("The Laugh Out");
        verify(filmDAO).findByExactName("Walking Out");
        verify(rentalDAO, times(2)).findByCustomerIdAndFilmID(0, 0);
    }

    // Test to check whether the returnRental resource is working correctly when an incorrectly formatted date is provided
    @Test
    public void returnRentalIncorrectDateTest() {
        ArrayList<FilmReturnDetails> filmReturnDetails = new ArrayList<>();

        filmReturnDetails.add(new FilmReturnDetails("Walking Out"));

        ReturnRental returnRental = new ReturnRental(1, "244/08/2017", filmReturnDetails);

        final Response rentalResponse = resources.client()
                .target("/rentals")
                .request()
                .post(Entity.entity(returnRental, MediaType.APPLICATION_JSON));

        assertEquals(rentalResponse.getStatus(), Response.Status.BAD_REQUEST.getStatusCode());
    }
}
