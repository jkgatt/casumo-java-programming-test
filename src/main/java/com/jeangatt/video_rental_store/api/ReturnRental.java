package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class ReturnRental extends AddOrReturnRental {

    // Arraylist containing the Film rental details
    @JsonProperty
    private ArrayList<FilmReturnDetails> films;

    public ReturnRental() {
        // Jackson deserialization
    }

    public ReturnRental(int customerID, String date, ArrayList<FilmReturnDetails> films) {
        super(customerID, date);
        this.films = films;
    }

    public ArrayList<FilmReturnDetails> getFilms() {
        return films;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        // Checking the super class first (customer ID and date)
        if(!super.equals(obj)){
            return false;
        }

        ReturnRental returnRental = (ReturnRental) obj;

        // Checking the films arraylist
        if(!films.equals(returnRental.films)){
            return false;
        }

        return true;
    }
}