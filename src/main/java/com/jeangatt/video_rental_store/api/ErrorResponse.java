package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {

    // HTTP error response code
    @JsonProperty("error_code")
    private int errorCode;

    // The error message
    @JsonProperty
    private String message;

    public ErrorResponse() {
        // Jackson Deserialization
    }

    public ErrorResponse(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        ErrorResponse errorResponse = (ErrorResponse) obj;

        if(errorCode != 0 ? errorCode != errorResponse.errorCode : errorResponse.errorCode != 0){
            return false;
        }

        if(message != null ? !message.equals(errorResponse.message) : errorResponse.message != null){
            return false;
        }

        return true;
    }
}
