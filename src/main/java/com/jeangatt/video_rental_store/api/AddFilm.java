package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddFilm {

    // The name of the film
    @JsonProperty("film_name")
    private String filmName;

    // The ID of the film type
    @JsonProperty("film_type")
    private int filmType;

    public AddFilm() {
        // Jackson deserialization
    }

    public AddFilm(String filmName, int filmType) {
        this.filmName = filmName;
        this.filmType = filmType;
    }

    public String getFilmName() {
        return filmName;
    }

    public int getFilmType() {
        return filmType;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        AddFilm addFilm = (AddFilm) obj;

        if(filmName != null ? !filmName.equals(addFilm.filmName) : addFilm.filmName != null){
            return false;
        }
        if(filmType != 0 ? filmType != addFilm.filmType : addFilm.filmType != 0){
            return false;
        }

        return true;
    }
}
