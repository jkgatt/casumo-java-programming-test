package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class AddOrReturnRental {

    // ID of the Customer who is going to rent the film/films
    @JsonProperty("customer_id")
    private int customerID;

    // The date of the rental or return
    @JsonProperty
    private String date;


    public AddOrReturnRental() {
        // Jackson deserialization
    }

    public AddOrReturnRental(int customerID, String date) {
        this.customerID = customerID;
        this.date = date;
    }

    public int getCustomerID() {
        return customerID;
    }

    public String getDate() {
        return date;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        AddOrReturnRental addOrReturnRental = (AddOrReturnRental) obj;

        if(customerID != 0 ? customerID != addOrReturnRental.customerID : addOrReturnRental.customerID != 0){
            return false;
        }

        if(date != null ? !date.equals(addOrReturnRental.date) : addOrReturnRental.date != null){
            return false;
        }

        return true;
    }
}
