package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class ResponseCalculations {

    // ID of the Customer who is going to rent the films
    @JsonProperty("customer_id")
    private int customerID;

    public ResponseCalculations() {
        // Jackson Deserialization
    }

    public ResponseCalculations(int customerID) {
        this.customerID = customerID;
    }

    public int getCustomerID() {
        return customerID;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        ResponseCalculations responseCalculations = (ResponseCalculations) obj;

        if(customerID != 0 ? customerID != responseCalculations.customerID : responseCalculations.customerID != 0){
            return false;
        }

        return true;
    }
}
