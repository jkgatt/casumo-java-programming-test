package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jeangatt.video_rental_store.core.FilmType;

import java.math.BigDecimal;

public class FilmRentalDetails extends FilmDetails {

    // The number of days that the Film will be rented for
    @JsonProperty
    private int days;

    // The cost of the rental
    @JsonProperty
    private BigDecimal cost;

    public FilmRentalDetails() {
        // Jackson deserialization
    }

    public FilmRentalDetails(String filmName, int days) {
        super(filmName);
        this.days = days;
    }

    public FilmRentalDetails(String filmName, int days, BigDecimal cost) {
        super(filmName);
        this.days = days;
        this.cost = cost;
    }

    public int getDays() {
        return days;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        // Checking the super class first (film name)
        if(!super.equals(obj)){
            return false;
        }

        FilmRentalDetails filmRentalDetails = (FilmRentalDetails) obj;

        if(days != 0 ? days != filmRentalDetails.days : filmRentalDetails.days != 0){
            return false;
        }

        if(cost != null ? !cost.equals(filmRentalDetails.cost) : filmRentalDetails.cost != null){
            return false;
        }

        return true;
    }
}
