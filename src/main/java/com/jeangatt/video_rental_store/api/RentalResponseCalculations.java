package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class RentalResponseCalculations extends ResponseCalculations {

    // Film Rental Details
    @JsonProperty
    private ArrayList<FilmRentalDetails> films;

    // Total Cost of the Film Rental
    @JsonProperty("total_cost")
    private double totalCost;

    public RentalResponseCalculations() {
        // Jackson Deserialization
    }

    public RentalResponseCalculations(int customerID, ArrayList<FilmRentalDetails> films) {
        super(customerID);
        this.films = films;
    }

    public RentalResponseCalculations(int customerID, ArrayList<FilmRentalDetails> films, double totalCost) {
        super(customerID);
        this.films = films;
        this.totalCost = totalCost;
    }

    public ArrayList<FilmRentalDetails> getFilms() {
        return films;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        // Checking the super class first (customer id)
        if(!super.equals(obj)){
            return false;
        }

        RentalResponseCalculations rentalResponseCalculations = (RentalResponseCalculations) obj;

        if(!films.equals(rentalResponseCalculations.films)){
            return false;
        }

        if(totalCost != 0 ? totalCost != rentalResponseCalculations.totalCost : rentalResponseCalculations.totalCost != 0){
            return false;
        }

        return true;
    }
}

