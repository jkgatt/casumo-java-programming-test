package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class AddRental extends AddOrReturnRental {

    // Arraylist containing the Film rental details
    @JsonProperty
    private ArrayList<FilmRentalDetails> films;

    public AddRental() {
        // Jackson deserialization
    }

    public AddRental(int customerID, String date, ArrayList<FilmRentalDetails> films) {
        super(customerID, date);
        this.films = films;
    }

    public ArrayList<FilmRentalDetails> getFilms() {
        return films;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        // Checking the super class first (customer ID and date)
        if(!super.equals(obj)){
            return false;
        }

        AddRental addRental = (AddRental) obj;

        // Checking the films arraylist
        if(!films.equals(addRental.films)){
            return false;
        }

        return true;
    }
}
