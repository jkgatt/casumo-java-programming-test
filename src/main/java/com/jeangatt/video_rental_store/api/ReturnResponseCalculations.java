package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class ReturnResponseCalculations extends ResponseCalculations {

    // Film Return Details
    @JsonProperty
    private ArrayList<FilmReturnDetails> films;

    // Total late charge of the Film Rental upon return
    @JsonProperty("total_late_charge")
    private double totalLateCharge;

    public ReturnResponseCalculations() {
        // Jackson Deserialization
    }

    public ReturnResponseCalculations(int customerID, ArrayList<FilmReturnDetails> films) {
        super(customerID);
        this.films = films;
    }

    public ReturnResponseCalculations(int customerID, ArrayList<FilmReturnDetails> films, double totalLateCharge) {
        super(customerID);
        this.films = films;
        this.totalLateCharge = totalLateCharge;
    }

    public ArrayList<FilmReturnDetails> getFilms() {
        return films;
    }

    public double getTotalLateCharge() {
        return totalLateCharge;
    }

    public void setTotalLateCharge(double totalLateCharge) {
        this.totalLateCharge = totalLateCharge;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        // Checking the super class first (customer id)
        if(!super.equals(obj)){
            return false;
        }

        ReturnResponseCalculations returnResponseCalculations = (ReturnResponseCalculations) obj;

        if(!films.equals(returnResponseCalculations.films)){
            return false;
        }

        if(totalLateCharge != 0 ? totalLateCharge != returnResponseCalculations.totalLateCharge : returnResponseCalculations.totalLateCharge != 0){
            return false;
        }

        return true;
    }
}
