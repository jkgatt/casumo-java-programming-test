package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class FilmReturnDetails extends FilmDetails {

    // The number of extra days that the Film was rented for
    @JsonProperty("extra_days")
    private int extraDays;

    // The extra cost of the rental
    @JsonProperty("extra_cost")
    private BigDecimal extraCost;

    public FilmReturnDetails() {
        // Jackson deserialization
    }

    public FilmReturnDetails(String filmName) {
        super(filmName);
    }

    public FilmReturnDetails(String filmName, int extraDays, BigDecimal extraCost) {
        super(filmName);
        this.extraDays = extraDays;
        this.extraCost = extraCost;
    }

    public int getExtraDays() {
        return extraDays;
    }

    public void setExtraDays(int extraDays) {
        this.extraDays = extraDays;
    }

    public BigDecimal getExtraCost() {
        return extraCost;
    }

    public void setExtraCost(BigDecimal extraCost) {
        this.extraCost = extraCost;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        // Checking the super class first (film name)
        if(!super.equals(obj)){
            return false;
        }

        FilmReturnDetails filmReturnDetails = (FilmReturnDetails) obj;

        if(extraDays != 0 ? extraDays != filmReturnDetails.extraDays : filmReturnDetails.extraDays != 0){
            return false;
        }

        if(extraCost != null ? !extraCost.equals(filmReturnDetails.extraCost) : filmReturnDetails.extraCost != null){
            return false;
        }

        return true;
    }
}