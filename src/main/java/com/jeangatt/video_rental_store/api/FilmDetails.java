package com.jeangatt.video_rental_store.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FilmDetails {

    // The name of the Film
    @JsonProperty("film_name")
    private String filmName;

    public FilmDetails() {
        // Jackson deserialization
    }

    public FilmDetails(String filmName) {
        this.filmName = filmName;
    }

    public String getFilmName() {
        return filmName;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        FilmDetails filmDetails = (FilmDetails) obj;

        if(filmName != null ? !filmName.equals(filmDetails.filmName) : filmDetails.filmName != null){
            return false;
        }

        return true;
    }
}
