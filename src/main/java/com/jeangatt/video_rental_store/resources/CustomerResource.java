package com.jeangatt.video_rental_store.resources;

import com.jeangatt.video_rental_store.core.Customer;
import com.jeangatt.video_rental_store.db.CustomerDAO;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.IntParam;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("/customers")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerResource {

    // Logger instance for logging
    private static final Logger LOGGER = LoggerFactory.getLogger(FilmResource.class);

    private CustomerDAO customerDAO;

    public CustomerResource(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    // Retrieving a customer by their Database ID
    @GET
    @Path("/{id}")
    @UnitOfWork
    public Optional<Customer> findById(@PathParam("id")IntParam id) {
        return customerDAO.findById(id.get());
    }

    // Adding a new Customer to the database
    @POST
    @UnitOfWork
    public Response addCustomer(){
        try {
            customerDAO.createOrUpdateCustomer(new Customer());
        } catch(HibernateException ex){
            return ResourceHelper.useErrorResponse(LOGGER, Response.Status.INTERNAL_SERVER_ERROR,
                    "Failed to create a new customer in the database");
        }
        return Response.status(Response.Status.OK).build();
    }
}
