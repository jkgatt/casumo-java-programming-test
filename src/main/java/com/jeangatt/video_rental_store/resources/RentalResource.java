package com.jeangatt.video_rental_store.resources;

import com.jeangatt.video_rental_store.api.*;
import com.jeangatt.video_rental_store.core.Customer;
import com.jeangatt.video_rental_store.core.Film;
import com.jeangatt.video_rental_store.core.Rental;
import com.jeangatt.video_rental_store.db.CustomerDAO;
import com.jeangatt.video_rental_store.db.FilmDAO;
import com.jeangatt.video_rental_store.db.RentalDAO;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.IntParam;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.DAYS;

@Path("/rentals")
@Produces(MediaType.APPLICATION_JSON)
public class RentalResource {

    // Logger instance for logging
    private static final Logger LOGGER = LoggerFactory.getLogger(RentalResource.class);

    // Date Format
    private final static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private RentalDAO rentalDAO;
    private FilmDAO filmDAO;
    private CustomerDAO customerDAO;

    public RentalResource(RentalDAO rentalDAO, FilmDAO filmDAO, CustomerDAO customerDAO) {
        this.rentalDAO = rentalDAO;
        this.filmDAO = filmDAO;
        this.customerDAO = customerDAO;
    }

    // Retrieve a rental by its database ID
    @GET
    @Path("/{id}")
    @UnitOfWork
    public Optional<Rental> findById(@PathParam("id") IntParam id) {
        return rentalDAO.findById(id.get());
    }

    // Add a new rental to the database
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response addRental(AddRental addRental) {

        // Rental date
        LocalDate rentalDate;

        // Rental response calculations
        RentalResponseCalculations rentalResponseCalculations = new RentalResponseCalculations(addRental.getCustomerID(),
                addRental.getFilms());

        // Details of the customer renting the films
        Optional<Customer> customer = customerDAO.findById(addRental.getCustomerID());
        if (!customer.isPresent()) {
            return ResourceHelper.useErrorResponse(LOGGER, Response.Status.NOT_FOUND,
                    "Didn't find the customer with the specified ID.");
        }

        // Parsing the string date passed to LocalDate
        try {
            rentalDate = LocalDate.parse(addRental.getDate(), dateFormat);
        } catch (DateTimeParseException ex) {
            return ResourceHelper.useErrorResponse(LOGGER, Response.Status.BAD_REQUEST,
                    "Couldn't parse the date passed because it isn't in the following format: dd/mm/yyyy.");
        }

        int bonusPoints = customer.get().getBonusPoints();
        double totalRentalCost = 0;
        int filmRentalCounter = 0;

        // Looping through every film that wants to be rented
        for (FilmDetails filmDetails : rentalResponseCalculations.getFilms()) {

            // Searching for the film details
            FilmRentalDetails currentFilmRentalDetails = rentalResponseCalculations.getFilms().get(filmRentalCounter);

            List<Film> films = filmDAO.findByExactName(currentFilmRentalDetails.getFilmName());
            if(films.size() == 0){
                return ResourceHelper.useErrorResponse(LOGGER, Response.Status.NOT_FOUND,
                        String.format("Didn't find any films with the specified name of %s.",
                                currentFilmRentalDetails.getFilmName()));
            }

            Film currentFilmDetails = films.get(0);

            // Checking if the film is available (therefore total rented out < availability)
            long filmsRentedOut = rentalDAO.totalRentalsByFilmId(currentFilmDetails.getId());
            if (filmsRentedOut >= currentFilmDetails.getQuantity()) {
                return ResourceHelper.useErrorResponse(LOGGER, Response.Status.FORBIDDEN,
                        String.format("The film (%s) cannot be rented out, as there are none available.",
                                currentFilmDetails.getName()));
            }

            // Cost of the film rental
            double rentalCost = 0;

            // The price of the film (Per day)
            double filmPrice = currentFilmDetails.getType().getPrice().getPrice().doubleValue();

            // Calculating the cost of the film rental, depending on the number of days and film type
            switch (currentFilmDetails.getType().getId()) {
                case 1:         // New Release
                    rentalCost = filmPrice * currentFilmRentalDetails.getDays();
                    bonusPoints += 2;
                    break;
                case 2:         // Regular Release
                    rentalCost = filmPrice;
                    if (currentFilmRentalDetails.getDays() > 3) {
                        rentalCost += filmPrice * (currentFilmRentalDetails.getDays() - 3);
                    }
                    bonusPoints++;
                    break;
                case 3:         // Old Release
                    rentalCost = filmPrice;
                    if (currentFilmRentalDetails.getDays() > 5) {
                        rentalCost += filmPrice * (currentFilmRentalDetails.getDays() - 5);
                    }
                    bonusPoints++;
                    break;
                default:
                    return ResourceHelper.useErrorResponse(LOGGER, Response.Status.INTERNAL_SERVER_ERROR,
                            "Didn't find the film type specified...");
            }

            // Setting the cost of the film rental for the response
            currentFilmRentalDetails.setCost(BigDecimal.valueOf(rentalCost));

            // Adding the cost of this rental to the total cost of the whole rental
            totalRentalCost += rentalCost;

            // Creating and setting up a new rental instance, and adding it to the database
            try {
                rentalDAO.createOrUpdate(new Rental(rentalDate, currentFilmRentalDetails.getDays(), BigDecimal.valueOf(rentalCost),
                        customer.get(), currentFilmDetails));
            } catch (HibernateException ex) {
                return ResourceHelper.useErrorResponse(LOGGER, Response.Status.INTERNAL_SERVER_ERROR,
                        "Failed to create and save one of the rentals to the database");
            }

            filmRentalCounter++;
        }

        // Setting the total cost for the response
        rentalResponseCalculations.setTotalCost(totalRentalCost);


        // Update the customer bonus points and persist it to the database
        customer.get().setBonusPoints(bonusPoints);
        try {
            customerDAO.createOrUpdateCustomer(customer.get());
        } catch (HibernateException ex) {
            return ResourceHelper.useErrorResponse(LOGGER, Response.Status.INTERNAL_SERVER_ERROR,
                    "Failed to save the updated bonus points for the customer specified");
        }

        return Response.status(Response.Status.OK).entity(rentalResponseCalculations).build();
    }

    // Updating the rental details in the database
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response returnRental(ReturnRental returnRental) {

        // Return date
        LocalDate returnDate;

        // Return response calculations
        ReturnResponseCalculations returnResponseCalculations = new ReturnResponseCalculations(
                returnRental.getCustomerID(), returnRental.getFilms());

        // Details of the customer renting the films
        Optional<Customer> customer = customerDAO.findById(returnRental.getCustomerID());
        if (!customer.isPresent()) {
            return ResourceHelper.useErrorResponse(LOGGER, Response.Status.NOT_FOUND,
                    "Didn't find the customer with the specified ID.");
        }

        // Parsing the string date to LocalDate
        try {
            returnDate = LocalDate.parse(returnRental.getDate(), dateFormat);
        } catch (DateTimeParseException ex) {
            return ResourceHelper.useErrorResponse(LOGGER, Response.Status.BAD_REQUEST,
                    "Couldn't parse the date passed because it isn't in the following format: dd/mm/yyyy.");
        }

        double totalExtraCharge = 0;
        int filmReturnCounter = 0;
        for (FilmDetails filmDetails : returnResponseCalculations.getFilms()) {

            // Searching for the film details
            FilmReturnDetails currentFilmReturnDetails = returnResponseCalculations.getFilms().get(filmReturnCounter);

            List<Film> films = filmDAO.findByExactName(currentFilmReturnDetails.getFilmName());
            if(films.size() == 0){
                return ResourceHelper.useErrorResponse(LOGGER, Response.Status.NOT_FOUND,
                        String.format("Didn't find any films with the specified name of %s.",
                                currentFilmReturnDetails.getFilmName()));
            }

            Film currentFilmDetails = films.get(0);

            // Searching for the rental details
            List<Rental> currentRentals = rentalDAO.findByCustomerIdAndFilmID(customer.get().getId(), currentFilmDetails.getId());
            if (currentRentals.size() == 0) {
                // The film rental wasn't found
                return ResourceHelper.useErrorResponse(LOGGER, Response.Status.NOT_FOUND,
                        String.format("No rentals where found for the customer with id of %d and the film name of: %s, " +
                                        "or it has already been returned.",
                                customer.get().getId(), currentFilmReturnDetails.getFilmName()));
            }

            // Getting the first rental from the list
            Rental currentRental = (Rental) currentRentals.get(0);

            // Checking that the specified return date is not before the rental date
            if(returnDate.isBefore(currentRental.getRentalDate())){
                return ResourceHelper.useErrorResponse(LOGGER, Response.Status.BAD_REQUEST,
                        String.format("The return date (%s) is before the rental date (%s) for the following film: " +
                                "(%s).", returnDate.toString(), currentRental.getRentalDate().toString(),
                        currentFilmReturnDetails.getFilmName()));
            }

            // Extra cost of the film rental
            double extraRentalCost = 0;

            // Checking if the film was rented for any extra days and calculating the extra cost
            int actualRentalDays = (int) DAYS.between(currentRental.getRentalDate(), returnDate);
            if(actualRentalDays > currentRental.getRentalDuration()){
                // Need to calculate and add the total cost of the rental

                int extraDays = actualRentalDays - currentRental.getRentalDuration();

                extraRentalCost = extraDays * currentFilmDetails.getType().getPrice().getPrice().doubleValue();
                double newRentalCost = currentRental.getTotalCost().doubleValue() + extraRentalCost;

                // Updating the total cost and number of days of the rental for the database
                currentRental.setTotalCost(BigDecimal.valueOf(newRentalCost));
                currentRental.setRentalDuration(actualRentalDays);

                // Updating the extra cost and number of days of the rental for the response
                currentFilmReturnDetails.setExtraCost(BigDecimal.valueOf(extraRentalCost));
                currentFilmReturnDetails.setExtraDays(extraDays);
            } else{
                // Set the extra cost and number of days of the rental to 0
                currentFilmReturnDetails.setExtraCost(BigDecimal.ZERO);
                currentFilmReturnDetails.setExtraDays(0);
            }

            // Adding the extra rental cost to the total extra rental cost
            totalExtraCharge += extraRentalCost;

            // Adding the return date to the database object
            currentRental.setReturnDate(returnDate);

            // Updating the rental instance in the database.
            try {
                rentalDAO.createOrUpdate(currentRental);
            } catch (HibernateException ex) {
                return ResourceHelper.useErrorResponse(LOGGER, Response.Status.INTERNAL_SERVER_ERROR,
                        String.format("Failed to update the rental (id: %d) in the database",
                                currentRental.getId()));
            }

            filmReturnCounter++;
        }

        // Setting the total late charge in the response
        returnResponseCalculations.setTotalLateCharge(totalExtraCharge);

        return Response.status(Response.Status.OK).entity(returnResponseCalculations).build();
    }

}
