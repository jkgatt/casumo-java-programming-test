package com.jeangatt.video_rental_store.resources;

import com.jeangatt.video_rental_store.api.ErrorResponse;
import org.slf4j.Logger;

import javax.ws.rs.core.Response;

public class ResourceHelper {

    // Used to create an error response by passing the logger, http status code and error message
    public static Response useErrorResponse(Logger logger, Response.Status httpStatus, String message){
        logger.error(message);
        ErrorResponse errorResponse = new ErrorResponse(httpStatus.getStatusCode(), message);
        return Response.status(httpStatus).entity(errorResponse).build();
    }
}
