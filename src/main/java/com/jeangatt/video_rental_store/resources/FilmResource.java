package com.jeangatt.video_rental_store.resources;

import com.jeangatt.video_rental_store.api.AddFilm;
import com.jeangatt.video_rental_store.core.Film;
import com.jeangatt.video_rental_store.core.FilmType;
import com.jeangatt.video_rental_store.db.FilmDAO;
import com.jeangatt.video_rental_store.db.FilmTypeDAO;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.IntParam;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Path("/films")
@Produces(MediaType.APPLICATION_JSON)
public class FilmResource {

    // Logger instance for logging
    private static final Logger LOGGER = LoggerFactory.getLogger(FilmResource.class);

    private FilmDAO filmDAO;
    private FilmTypeDAO filmTypeDAO;

    public FilmResource(FilmDAO filmDAO, FilmTypeDAO filmTypeDAO) {
        this.filmDAO = filmDAO;
        this.filmTypeDAO = filmTypeDAO;
    }

    // Retrieving a film by its database ID
    @GET
    @Path("/{id}")
    @UnitOfWork
    public Optional<Film> findById(@PathParam("id")IntParam id){
        return filmDAO.findById(id.get());
    }

    // Adding a film to the database
    @POST
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addFilm(AddFilm filmParam){

        Film film;

        List<Film> films = filmDAO.findByExactName(filmParam.getFilmName());

        // Checking the size of films list
        if(films.size() != 0){
            //Taking the first film found in the list
            film = films.get(0);
            // Add 1 to the quantity
            LOGGER.info(String.format("The film with the name: %s, is already found in the database, " +
                    "adding one to the quantity.", filmParam.getFilmName()));
            film.setQuantity(film.getQuantity() + 1);
        } else {
            film = new Film(filmParam.getFilmName());

            // Getting and setting the film type
            Optional<FilmType> filmType = filmTypeDAO.findById(filmParam.getFilmType());
            if (filmType.isPresent()) {
                film.setType(filmType.get());
            } else {
                return ResourceHelper.useErrorResponse(LOGGER, Response.Status.NOT_FOUND,
                        "The film type with the id: %d, isn't found in the database.");
            }
        }

        // Saving the film or updating the quantity to the database
        try{
            filmDAO.createOrUpdate(film);
        } catch(HibernateException ex){
             return ResourceHelper.useErrorResponse(LOGGER, Response.Status.INTERNAL_SERVER_ERROR,
                     String.format("An exception has occured while trying to create a new Film, " +
                             "exception message: %s", ex.getMessage()));
        }

        return Response.ok().build();
    }
}
