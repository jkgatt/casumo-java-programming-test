package com.jeangatt.video_rental_store;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class VideoRentalStoreConfiguration extends Configuration{

    // A database configuration factory
    @Valid
    @NotNull
    @JsonProperty("database")
    private DataSourceFactory databaseConfiguration = new DataSourceFactory();

    /**
     * A getter for the database factory, to be able to return the
     * deserialized data from the configuration file.
     */
    public DataSourceFactory getDataSourceFactory(){
        return databaseConfiguration;
    }

}
