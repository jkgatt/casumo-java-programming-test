package com.jeangatt.video_rental_store.core;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "film_price")
public class FilmPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private BigDecimal price;

    public FilmPrice() {
        // Jackson deserialization
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }

        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        FilmPrice filmPrice = (FilmPrice) obj;

        if(id != 0 ? id != filmPrice.id : filmPrice.id != 0){
            return false;
        }

        if(name != null ? name != filmPrice.name : filmPrice.name != null){
            return false;
        }

        if(price !=  null ? !price.equals(filmPrice.price) : filmPrice.price != null){
            return false;
        }

        return true;
    }
}
