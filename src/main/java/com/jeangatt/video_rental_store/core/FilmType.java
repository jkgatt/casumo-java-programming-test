package com.jeangatt.video_rental_store.core;

import javax.persistence.*;

@Entity
@Table(name = "film_type")
public class FilmType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "price_id")
    private FilmPrice price;

    public FilmType() {
        // Jackson deserialization
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FilmPrice getPrice() {
        return price;
    }

    public void setPrice(FilmPrice price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }

        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        FilmType filmType = (FilmType) obj;

        if(id != 0 ? id != filmType.id : filmType.id != 0){
            return false;
        }

        if(name != null ? name != filmType.name : filmType.name != null){
            return false;
        }

        if(!price.equals(filmType.price)){
            return false;
        }

        return true;
    }
}
