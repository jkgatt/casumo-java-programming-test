package com.jeangatt.video_rental_store.core;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "film")
@NamedQueries({
        @NamedQuery(name = "com.jeangatt.video_rental_store.core.Film.findByName",
                query = "SELECT f FROM Film f where f.name = :name")
})
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "quantity")
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private FilmType type;

    public Film() {
        // Jackson deserialization
    }

    public Film(String name) {
        this.name = name;
        this.quantity = 1;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public FilmType getType() {
        return type;
    }

    public void setType(FilmType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }

        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        Film film = (Film) obj;

        if(id != 0 ? id != film.id : film.id != 0){
            return false;
        }

        if(name != null ? name != film.name : film.name != null){
            return false;
        }

        if(quantity != 0 ? quantity != film.quantity : film.quantity != 0){
            return false;
        }

        if(!type.equals(film.type)){
            return false;
        }

        return true;
    }
}
