package com.jeangatt.video_rental_store.core;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "bonus_points")
    private int bonusPoints;

    public Customer() {
        // Jackson deserialization
    }

    public int getId() {
        return id;
    }

    public int getBonusPoints() {
        return bonusPoints;
    }

    public void setBonusPoints(int bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }

        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        Customer customer = (Customer) obj;

        if(id != 0 ? id != customer.id : customer.id != 0){
            return false;
        }

        if(bonusPoints != 0 ? bonusPoints != customer.bonusPoints : customer.bonusPoints != 0){
            return false;
        }

        return true;
    }
}
