package com.jeangatt.video_rental_store.core;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "rental")
@NamedQueries({
        @NamedQuery(name = "com.jeangatt.video_rental_store.core.Rental.findTotalCurrentlyRented",
                query = "SELECT COUNT(r) FROM Rental r WHERE film.id = :film_id AND returnDate IS NULL"),
        @NamedQuery(name = "com.jeangatt.video_rental_store.core.Rental.findByCustomerIDAndFilmID",
                query = "SELECT r FROM Rental r WHERE customer.id = :customer_id AND film.id = :film_id AND returnDate IS NULL")
})
public class Rental {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "rental_date")
    private LocalDate rentalDate;

    @Column(name = "rental_duration")
    private int rentalDuration;

    @Column(name = "return_date")
    private LocalDate returnDate;

    @Column(name = "total_cost")
    private BigDecimal totalCost;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "film_id")
    private Film film;

    public Rental() {
        // Jackson deserialization
    }

    public Rental(LocalDate rentalDate, int rentalDuration, BigDecimal totalCost, Customer customer, Film film) {
        this.rentalDate = rentalDate;
        this.rentalDuration = rentalDuration;
        this.totalCost = totalCost;
        this.customer = customer;
        this.film = film;
    }

    public int getId() {
        return id;
    }

    public int getRentalDuration() {
        return rentalDuration;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Film getFilm() {
        return film;
    }

    public LocalDate getRentalDate() {
        return rentalDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public void setRentalDuration(int rentalDuration) {
        this.rentalDuration = rentalDuration;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }

        if(obj == null || getClass() != obj.getClass()){
            return false;
        }

        Rental rental = (Rental) obj;

        if(id != 0 ? id != rental.id : rental.id != 0){
            return false;
        }

        if(rentalDate != null ? !rentalDate.equals(rental.rentalDate) : rental.rentalDate != null){
            return false;
        }

        if(rentalDuration != 0 ? rentalDuration != rental.rentalDuration : rental.rentalDuration != 0){
            return false;
        }

        if(returnDate != null ? !returnDate.equals(rental.returnDate) : rental.returnDate != null){
            return false;
        }

        if(!customer.equals(rental.customer)){
            return false;
        }

        if(!film.equals(rental.film)){
            return false;
        }

        return true;
    }
}
