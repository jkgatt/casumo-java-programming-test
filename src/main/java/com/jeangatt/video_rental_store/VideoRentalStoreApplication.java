package com.jeangatt.video_rental_store;

import com.jeangatt.video_rental_store.core.*;
import com.jeangatt.video_rental_store.db.CustomerDAO;
import com.jeangatt.video_rental_store.db.FilmDAO;
import com.jeangatt.video_rental_store.db.FilmTypeDAO;
import com.jeangatt.video_rental_store.db.RentalDAO;
import com.jeangatt.video_rental_store.resources.CustomerResource;
import com.jeangatt.video_rental_store.resources.FilmResource;
import com.jeangatt.video_rental_store.resources.RentalResource;
import io.dropwizard.Application;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class VideoRentalStoreApplication extends Application<VideoRentalStoreConfiguration> {

    public static void main(String[] args) throws Exception {
        new VideoRentalStoreApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<VideoRentalStoreConfiguration> bootstrap) {
        super.initialize(bootstrap);
        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(VideoRentalStoreConfiguration configuration, Environment environment) throws Exception {

        // Initialising the DAO's
        final FilmDAO filmDAO = new FilmDAO(hibernateBundle.getSessionFactory());
        final FilmTypeDAO filmTypeDAO = new FilmTypeDAO(hibernateBundle.getSessionFactory());
        final CustomerDAO customerDAO = new CustomerDAO(hibernateBundle.getSessionFactory());
        final RentalDAO rentalDAO = new RentalDAO(hibernateBundle.getSessionFactory());

        // Registering all the resources
        environment.jersey().register(new FilmResource(filmDAO, filmTypeDAO));
        environment.jersey().register(new CustomerResource(customerDAO));
        environment.jersey().register(new RentalResource(rentalDAO, filmDAO, customerDAO));
    }

    // HibernateBundle where the entity classes, as well as how to get the database config, is specified
    private final HibernateBundle<VideoRentalStoreConfiguration> hibernateBundle =
            new HibernateBundle<VideoRentalStoreConfiguration>(
                    FilmPrice.class,
                    FilmType.class,
                    Film.class,
                    Customer.class,
                    Rental.class
            ) {
                @Override
                public PooledDataSourceFactory getDataSourceFactory(VideoRentalStoreConfiguration videoRentalStoreConfiguration) {
                    return videoRentalStoreConfiguration.getDataSourceFactory();
                }
            };
}
