package com.jeangatt.video_rental_store.db;

import com.jeangatt.video_rental_store.core.Rental;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Optional;

public class RentalDAO extends AbstractDAO<Rental>{

    public RentalDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    // Creating a rental or updating its details
    public int createOrUpdate(Rental rental) throws HibernateException{
        return persist(rental).getId();
    }

    // Finding a rental by its database ID
    public Optional<Rental> findById(int id) {
        return Optional.ofNullable(get(id));
    }

    // Retrieving the number of rentals that are currently rented out by their Film ID
    public long totalRentalsByFilmId(int filmId) {
        return (long) list(namedQuery("com.jeangatt.video_rental_store.core.Rental.findTotalCurrentlyRented")
                .setParameter("film_id", filmId)).get(0);
    }

    // Finding rentals with a customer ID and a film ID
    public List<Rental> findByCustomerIdAndFilmID(int customerId, int filmId){
        return list(namedQuery("com.jeangatt.video_rental_store.core.Rental.findByCustomerIDAndFilmID")
                .setParameter("customer_id", customerId).setParameter("film_id", filmId));
    }
}
