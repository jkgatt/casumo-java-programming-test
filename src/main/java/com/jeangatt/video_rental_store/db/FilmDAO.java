package com.jeangatt.video_rental_store.db;

import com.jeangatt.video_rental_store.core.Film;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class FilmDAO extends AbstractDAO<Film> {

    public FilmDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    // Create a Film or update its details
    public int createOrUpdate(Film film) throws HibernateException {
        return persist(film).getId();
    }

    // Find a film by its database ID
    public Optional<Film> findById(int id) {
        return Optional.ofNullable(get(id));
    }

    // Find Films by their Wilcard Name
    public List<Film> findByWildcardName(String name) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("%").append(name.trim()).append("%");
        return list(namedQuery("com.jeangatt.video_rental_store.core.Film.findByName")
                .setParameter("name", stringBuilder.toString()));
    }

    // Find Films by their Exact Name
    public List<Film> findByExactName(String name) {
        return list(namedQuery("com.jeangatt.video_rental_store.core.Film.findByName")
                .setParameter("name", name));
    }
}