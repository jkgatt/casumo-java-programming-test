package com.jeangatt.video_rental_store.db;

import com.jeangatt.video_rental_store.core.FilmType;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.Optional;

public class FilmTypeDAO extends AbstractDAO<FilmType> {

    public FilmTypeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    // Searching for a film type by its database ID
    public Optional<FilmType> findById(int id) {
        return Optional.ofNullable(get(id));
    }
}
