package com.jeangatt.video_rental_store.db;

import com.jeangatt.video_rental_store.core.Customer;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;

import java.util.Optional;

public class CustomerDAO extends AbstractDAO<Customer> {

    public CustomerDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    // Create a customer or update their details
    public int createOrUpdateCustomer(Customer customer) throws HibernateException {
        return persist(customer).getId();
    }

    // Find a customer by their database ID
    public Optional<Customer> findById(int id) {
        return Optional.ofNullable(get(id));
    }
}
