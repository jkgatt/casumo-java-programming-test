# Casumo Java Programming Test - Video Rental Store

This is a programming test given to me by Casumo, where a Video Rental Store API needs to be designed and developed, so that my Java coding skills, structing and API design can be assesed.

## What was focused on
* Database Design
* API Design
* Code Structuring
* Unit Testing
	* Please note that although I tried my very best to create good tests, testing isn't my forte and if we ever move forward, I would require some mentoring on how to create better tests, thank you.

## Assumptions
* The rental days are not calculated using date and time, but just the date.
* When a film is being returned by a client who has rented the same film twice, the first one added to the database is set as returned.
* The currency will not be changed and SEK will be used.

## Getting Started
1. Download or clone the source code
2. The MySQL database needs to be created and setup by running the statements in the *sql/statements* folder in the following order:
	* create_schema_and_tables.sql
	* create_db_user_and_grant_privileges.sql
	* film_type_and_film_price_data.sql
	* add_mock_data.sql
3. Run **mvn clean install**, to compile, build and install
4. Run the compiled .jar file, adding the following arguments at the end **server configuration.yml**

## Usage
The following REST resources are provided:

* POST:		/customers (com.jeangatt.video_rental_store.resources.CustomerResource)
	* Creates a new customer in the database
* GET:		/customers/{id} (com.jeangatt.video_rental_store.resources.CustomerResource)
	* Retrieves a customer from the database, depending on the ID passed
* POST:		/films (com.jeangatt.video_rental_store.resources.FilmResource)
	* Creates a new film in the database, or updates it's quantity if it is already in the database.
	* Request body as JSON:
	```
	{
            "film_name": "Test12345", // Name of the film
            "film_type": 1 // ID of the film type
	}
	```
* GET:		/films/{id} (com.jeangatt.video_rental_store.resources.FilmResource)
	* Retrieves a film from the database, depending on the ID passed
* POST:		/rentals (com.jeangatt.video_rental_store.resources.RentalResource)
	* Creates a new rental in the database, returning the calculated costs of the rental
	* Request body as JSON:
	```
	{
            "customer_id": 3, // ID of the customer
            "date": "20/06/2017", // Rental date
            "films":        // Film names and number of rental days
            [ 
                {
                    "film_name": "Dunkirk",
                    "days": 2
                },
                {
                    "film_name": "Get Out",
                    "days": 10
                }
            ]
	}
	```
* PUT:		/rentals (com.jeangatt.video_rental_store.resources.RentalResource)
	* Updates a rental in the database, returning any extra cost that needs to be paid.
	* Request body as JSON:
	```
	{
            "customer_id": 1, // ID of the customer
            "date": "15/07/2017", // Return date
            "films":        // Film names
            [
                {
                    "film_name": "Dunkirk"
                },
                {
                    "film_name": "Get Out"
                }
            ]
	}
	```
* GET:		/rentals/{id} (com.jeangatt.video_rental_store.resources.RentalResource)
	* Retrieves a rental from the database, depending on the ID passed
